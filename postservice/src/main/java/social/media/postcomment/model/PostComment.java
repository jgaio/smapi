package social.media.postcomment.model;

import java.io.Serializable;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostComment implements Serializable{

	@JsonProperty("id")
	@Id
	private String id = null;

	@JsonProperty("email")
	private String email = null;

	@JsonProperty("status")
	private String status = null;
	
	@JsonProperty("dateCreated")
	private String dateCreated = null;
	
	@JsonProperty("comment")
	private String comment = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
}
