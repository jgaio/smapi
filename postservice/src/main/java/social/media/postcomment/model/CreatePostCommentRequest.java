package social.media.postcomment.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePostCommentRequest implements Serializable{

	@JsonProperty("email")
	private String email = null;
	
	@JsonProperty("comment")
	private String comment = null;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
}
