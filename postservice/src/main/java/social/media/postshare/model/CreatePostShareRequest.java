package social.media.postshare.model;

import java.io.Serializable;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePostShareRequest implements Serializable{
	
	@JsonProperty("postId")
	@Id
	private String postId = null;

	@JsonProperty("email")
	private String email = null;
	
	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
