package social.media.postshare.model;

import java.io.Serializable;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostShare implements Serializable{

	@JsonProperty("id")
	@Id
	private String id = null;

	@JsonProperty("email")
	private String email = null;

	@JsonProperty("postId")
	private String postId = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

}
