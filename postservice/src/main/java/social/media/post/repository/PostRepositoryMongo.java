package social.media.post.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import social.media.post.model.Post;

public interface PostRepositoryMongo extends MongoRepository <Post, Long>{

    public Post findByEmail(String email);
    //public List<Account> findByName(String name);
}