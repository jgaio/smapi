package social.media.post.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePostResponse implements Serializable{

	@JsonProperty("email")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
