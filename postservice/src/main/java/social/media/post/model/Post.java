package social.media.post.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Post implements Serializable{
	
	@JsonProperty("id")
	@Id
	private String id = null;

	@JsonProperty("email")
	private String email = null;

	@JsonProperty("status")
	private String status = null;
	
	@JsonProperty("dateCreated")
	private String dateCreated = null;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	  @Override
	  public boolean equals(java.lang.Object o) {
	    if (this == o) {
	      return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    Post account = (Post) o;
	    return Objects.equals(this.id, account.id) &&
	        Objects.equals(this.email, account.email);
	  }

	  @Override
	  public int hashCode() {
	    return Objects.hash(id, email);
	  }

	//  @Override
	//  public String toString() {
//	    StringBuilder sb = new StringBuilder();
//	    sb.append("class Account {\n");
	//    
//	    sb.append("    id: ").append(toIndentedString(id)).append("\n");
//	    sb.append("    email: ").append(toIndentedString(email)).append("\n");
//	    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
//	    sb.append("}");
//	    return sb.toString();
	//  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }
}
