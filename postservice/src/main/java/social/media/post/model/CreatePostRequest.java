package social.media.post.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePostRequest {
	
	@JsonProperty("accountId")
	private String accountId = null;
	
	@JsonProperty("textPosted")
	private String textPosted = null;

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getTextPosted() {
		return textPosted;
	}

	public void setTextPosted(String textPosted) {
		this.textPosted = textPosted;
	}

	public String getAccountId() {
		return accountId;
	}

	@Override
	  public boolean equals(java.lang.Object o) {
	    if (this == o) {
	      return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    CreatePostRequest createPostRequest = (CreatePostRequest) o;
	    return Objects.equals(this.accountId, createPostRequest.accountId);
	  }

	  @Override
	  public int hashCode() {
	    return Objects.hash(accountId);
	  }

	  @Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class CreatePostRequest {\n");
	    
	    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
	    sb.append("    textPosted: ").append(toIndentedString(textPosted)).append("\n");
	    sb.append("}");
	    return sb.toString();
	  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }
}
