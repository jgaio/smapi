package social.media.post.api;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.ApiParam;
import social.media.post.model.CreatePostRequest;
import social.media.post.model.CreatePostResponse;
import social.media.post.model.Post;
import social.media.post.model.TimelinePostsResponse;
import social.media.post.repository.PostRepositoryMongo;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-03T23:04:21.205Z")

@Controller
public class PostsApiController implements PostsApi {
	
	@Autowired
	private PostRepositoryMongo mrepository;
	
    //@Autowired
    //private RabbitTemplate template;

    //@Autowired
    //private FanoutExchange fanout;
    
    public ResponseEntity<CreatePostResponse> addPost(@ApiParam(value = "Request to add an account"  ) @RequestBody CreatePostRequest createPostRequest) {
    	//checking for account existing
    	//Iterable<Post> list = mrepository.findAll();
    	boolean existingAccount = false;
    	
    	if(!existingAccount){
	        // create and fill account object
	    	Post post = new Post();
	    	post.setEmail(createPostRequest.getTextPosted());
	    	
	    	//save on mongo
	    	mrepository.save(post);
	    	//Json j =  (Json) account;
	    	//enqueue - throw event - account created 	
	    	
			//template.convertAndSend(fanout.getName(),"", post);
			///System.out.println(" [x] Sent '" + createPostRequest.getName() + createAccountRequest.getEmail() + "'");
			
	    	CreatePostResponse response = new CreatePostResponse();
	    	//response.setEmail("as");
	        return new ResponseEntity<CreatePostResponse>(response,HttpStatus.CREATED);
	        
    	}else{
    		return new ResponseEntity<CreatePostResponse>(HttpStatus.CONFLICT);
    	}
    }

    
	public ResponseEntity<Post> getPostByEmail(@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail") String accountEmail,
			@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail2") String accountEmail2) {
        // do some magic!    	
        return new ResponseEntity<Post>(HttpStatus.OK);
    }

    public ResponseEntity<List<Post>> listAccounts() {
        // do some magic!
    	List<Post> result = (List<Post>) mrepository.findAll();
    	if (result == null){
    		result = new ArrayList<Post>();
    	}
        return new ResponseEntity<List<Post>>(result,HttpStatus.OK);
    }
    
    public ResponseEntity<Post> updatePostContent(){
    	return null;
    }


	@Override
	public ResponseEntity<List<TimelinePostsResponse>> getTimelinePosts() {
		// TODO Auto-generated method stub
		return null;
	}

}
