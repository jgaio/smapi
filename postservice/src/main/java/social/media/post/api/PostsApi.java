package social.media.post.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import social.media.post.model.CreatePostRequest;
import social.media.post.model.CreatePostResponse;
import social.media.post.model.Post;
import social.media.post.model.TimelinePostsResponse;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-03T23:04:21.205Z")

@Api(value = "posts")
public interface PostsApi {

    @ApiOperation(value = "adds an post", notes = "Adds an post to the system", response = CreatePostResponse.class, tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "post created", response = CreatePostResponse.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = CreatePostResponse.class),
        @ApiResponse(code = 409, message = "an existing post already exists", response = CreatePostResponse.class) })
    @RequestMapping(value = "/posts",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<CreatePostResponse> addPost(@ApiParam(value = "Request to add an post"  ) @RequestBody CreatePostRequest createPostRequest);


    @ApiOperation(value = "Find my posts", notes = "Returns a single post", response = Post.class, tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Post.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied", response = Post.class),
        @ApiResponse(code = 404, message = "post not found", response = Post.class) })
    @RequestMapping(value = "/posts/{accountEmail}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Post> getPostByEmail(@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail") String accountEmail,
    		@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail2") String accountEmail2);


    @ApiOperation(value = "list posts", notes = "Retrieve post list ", response = Post.class, responseContainer = "List", tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "posts", response = Post.class),
        @ApiResponse(code = 400, message = "bad input parameter", response = Post.class) })
    @RequestMapping(value = "/posts",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Post>> listAccounts();


    @ApiOperation(value = "timeline posts", notes = "Retrieve post list ", response = TimelinePostsResponse.class, responseContainer = "List", tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "posts", response = TimelinePostsResponse.class),
        @ApiResponse(code = 400, message = "bad input parameter", response = TimelinePostsResponse.class) })
    @RequestMapping(value = "/tlposts/{accountEmail}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<TimelinePostsResponse>> getTimelinePosts();
}
