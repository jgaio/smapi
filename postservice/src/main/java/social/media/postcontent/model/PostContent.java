package social.media.postcontent.model;

import java.io.Serializable;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PostContent implements Serializable{

	@JsonProperty("id")
	@Id
	private String id = null;

	@JsonProperty("email")
	private String email = null;

	@JsonProperty("status")
	private String status = null;
	
	@JsonProperty("dateCreated")
	private String dateCreated = null;
	
	@JsonProperty("content")
	private String content = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
