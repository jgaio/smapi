package social.media.postcontent.model;

import java.io.Serializable;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePostContentRequest implements Serializable{

	@JsonProperty("id")
	@Id
	private String id = null;

	@JsonProperty("email")
	private String email = null;
	
	@JsonProperty("content")
	private String content = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
