package social.media.profile.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import social.media.servicesharedmodels.account.Account;

/**
 * Profile
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-08T17:48:46.526Z")

@Entity
public class Profile implements Serializable  {
	@JsonProperty("email")
	private String email = null;

	@JsonProperty("id")
	@Id
	private String id = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("lastname")
	private String lastname = null;

	@JsonProperty("birthDate")
	private String birthDate = null;

	@JsonProperty("dateCreated")
	private String dateCreated = null;

	@JsonProperty("nickname")
	private String nickname = null;

	@JsonProperty("relationship")
	private String relationship = null;

	@JsonProperty("gender")
	private String gender = null;

	@JsonProperty("website")
	private String website = null;

	@JsonProperty("currentCity")
	private String currentCity = null;

	@JsonProperty("homeTown")
	private String homeTown = null;

	@JsonProperty("dateChanged")
	private String dateChanged = null;

	@JsonProperty("status")
	private String status = null;
	
	@JsonProperty("account")
	private Account account = null;
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}

	public Profile email(String email) {
		this.email = email;
		return this;
	}
	/**
	 * Get id
	 * @return id
	 **/
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	/**
	 * Get email
	 * @return email
	 **/
	@ApiModelProperty(example = "teste@gmail.com", required = true, value = "")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Profile name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name
	 * @return name
	 **/
	@ApiModelProperty(example = "Christian Roger", required = true, value = "")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Profile lastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	/**
	 * Get lastname
	 * @return lastname
	 **/
	@ApiModelProperty(example = "Gaio", required = true, value = "")
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Profile birthDate(String birthDate) {
		this.birthDate = birthDate;
		return this;
	}

	/**
	 * Get birthDate
	 * @return birthDate
	 **/
	@ApiModelProperty(example = "2016-08-29T09:12:33.001Z", required = true, value = "")
	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Profile dateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
		return this;
	}

	/**
	 * Get dateCreated
	 * @return dateCreated
	 **/
	@ApiModelProperty(example = "2016-08-29T09:12:33.001Z", value = "")
	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Profile nickname(String nickname) {
		this.nickname = nickname;
		return this;
	}

	/**
	 * Get nickname
	 * @return nickname
	 **/
	@ApiModelProperty(example = "cabeça", value = "")
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Profile relationship(String relationship) {
		this.relationship = relationship;
		return this;
	}

	/**
	 * Get relationship
	 * @return relationship
	 **/
	@ApiModelProperty(example = "solteiro", value = "")
	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public Profile gender(String gender) {
		this.gender = gender;
		return this;
	}

	/**
	 * Get gender
	 * @return gender
	 **/
	@ApiModelProperty(example = "masculino", value = "")
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Profile website(String website) {
		this.website = website;
		return this;
	}

	/**
	 * Get website
	 * @return website
	 **/
	@ApiModelProperty(example = "www.google.com", value = "")
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Profile currentCity(String currentCity) {
		this.currentCity = currentCity;
		return this;
	}

	/**
	 * Get currentCity
	 * @return currentCity
	 **/
	@ApiModelProperty(example = "Rio de Janeiro", value = "")
	public String getCurrentCity() {
		return currentCity;
	}

	public void setCurrentCity(String currentCity) {
		this.currentCity = currentCity;
	}

	public Profile homeTown(String homeTown) {
		this.homeTown = homeTown;
		return this;
	}

	/**
	 * Get homeTown
	 * @return homeTown
	 **/
	@ApiModelProperty(example = "Florianopolis", value = "")
	public String getHomeTown() {
		return homeTown;
	}

	public void setHomeTown(String homeTown) {
		this.homeTown = homeTown;
	}

	public Profile dateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
		return this;
	}

	/**
	 * Get dateChanged
	 * @return dateChanged
	 **/
	@ApiModelProperty(example = "2016-08-29T09:12:33.001Z", value = "")
	public String getDateChanged() {
		return dateChanged;
	}

	public void setDateChanged(String dateChanged) {
		this.dateChanged = dateChanged;
	}

	public Profile status(String status) {
		this.status = status;
		return this;
	}

	/**
	 * Get status
	 * @return status
	 **/
	@ApiModelProperty(example = "ativo", value = "")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Profile profile = (Profile) o;
		return Objects.equals(this.email, profile.email) &&
				Objects.equals(this.name, profile.name) &&
				Objects.equals(this.lastname, profile.lastname) &&
				Objects.equals(this.birthDate, profile.birthDate) &&
				Objects.equals(this.dateCreated, profile.dateCreated) &&
				Objects.equals(this.nickname, profile.nickname) &&
				Objects.equals(this.relationship, profile.relationship) &&
				Objects.equals(this.gender, profile.gender) &&
				Objects.equals(this.website, profile.website) &&
				Objects.equals(this.currentCity, profile.currentCity) &&
				Objects.equals(this.homeTown, profile.homeTown) &&
				Objects.equals(this.dateChanged, profile.dateChanged) &&
				Objects.equals(this.status, profile.status);
	}

	@Override
	public int hashCode() {
		return Objects.hash(email, name, lastname, birthDate, dateCreated, nickname, relationship, gender, website, currentCity, homeTown, dateChanged, status);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Profile {\n");

		sb.append("    email: ").append(toIndentedString(email)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
		sb.append("    birthDate: ").append(toIndentedString(birthDate)).append("\n");
		sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
		sb.append("    nickname: ").append(toIndentedString(nickname)).append("\n");
		sb.append("    relationship: ").append(toIndentedString(relationship)).append("\n");
		sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
		sb.append("    website: ").append(toIndentedString(website)).append("\n");
		sb.append("    currentCity: ").append(toIndentedString(currentCity)).append("\n");
		sb.append("    homeTown: ").append(toIndentedString(homeTown)).append("\n");
		sb.append("    dateChanged: ").append(toIndentedString(dateChanged)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

