package social.media.profile.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import social.media.profile.model.Profile;

@Repository
public interface ProfileRepository extends MongoRepository <Profile,Long>{

}
