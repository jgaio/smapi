package social.media.profile.api;

import io.swagger.annotations.*;
import social.media.profile.model.Profile;
import social.media.profile.repository.ProfileRepository;
import social.media.servicesharedmodels.account.Account;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-08T17:48:46.526Z")

@Controller
public class ProfileApiController implements ProfileApi {
	
	@Autowired
	private ProfileRepository repository;
	
	@Autowired
	private RabbitTemplate template;
	
    public ResponseEntity<Void> addProfile(@ApiParam(value = "Profile to add"  ) @RequestBody Profile profile) {
        // do some magic!
    	profile.setId("1");
    	repository.save(profile);
    	//template.convertAndSend(queue.getName(), profile);
    	System.out.println("deu certo" );
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Account> getProfileByEmail(@ApiParam(value = "account email to find by profile",required=true ) @PathVariable("accountEmail") String accountEmail) {
        // do some magic!
    	Account a = new Account();
    	a.setEmail(accountEmail);
        return new ResponseEntity<Account>(HttpStatus.OK);
    }

    public ResponseEntity<List<Profile>> searchProfile(@ApiParam(value = "pass an optional search string for looking up profile") @RequestParam(value = "searchString", required = false) String searchString,
        @ApiParam(value = "number of records to skip for pagination") @RequestParam(value = "skip", required = false) Integer skip,
        @ApiParam(value = "maximum number of records to return") @RequestParam(value = "limit", required = false) Integer limit) {
        // do some magic!
    	
    	List<Profile> listaProfile = (List<Profile>) repository.findAll();
    	
        return new ResponseEntity<List<Profile>>(listaProfile, HttpStatus.OK);
    }

}
