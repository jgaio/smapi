package social.media.profile.api;

import io.swagger.annotations.*;
import social.media.profile.model.Profile;
import social.media.servicesharedmodels.account.Account;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-08T17:48:46.526Z")

@Api(value = "profiles", description = "the profile API")
public interface ProfileApi {

    @ApiOperation(value = "adds a profile", notes = "Adds a profile to the system", response = Void.class, tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "profile created", response = Void.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = Void.class),
        @ApiResponse(code = 409, message = "an existing profile already exists", response = Void.class) })
    @RequestMapping(value = "/profiles",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> addProfile(@ApiParam(value = "Profile to add"  ) @RequestBody Profile profile);


    @ApiOperation(value = "Find profile by email", notes = "Returns a single profile", response = Profile.class, tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Profile.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied", response = Profile.class),
        @ApiResponse(code = 404, message = "Prodile not found", response = Profile.class) })
    @RequestMapping(value = "/profiles/{accountEmail}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Account> getProfileByEmail(@ApiParam(value = "account email to find by profile",required=true ) @PathVariable("accountEmail") String accountEmail);


    @ApiOperation(value = "searches profile", notes = "By passing in the appropriate options, you can search for available profile in the system ", response = Profile.class, responseContainer = "List", tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "search results matching criteria", response = Profile.class),
        @ApiResponse(code = 400, message = "bad input parameter", response = Profile.class) })
    @RequestMapping(value = "/profiles",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Profile>> searchProfile(@ApiParam(value = "pass an optional search string for looking up profile") @RequestParam(value = "searchString", required = false) String searchString,
        @ApiParam(value = "number of records to skip for pagination") @RequestParam(value = "skip", required = false) Integer skip,
        @ApiParam(value = "maximum number of records to return") @RequestParam(value = "limit", required = false) Integer limit);

}
