package social.media.profile.rabbit;

import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import social.media.profile.model.Profile;
import social.media.profile.repository.ProfileRepository;
import social.media.servicesharedmodels.account.Account;


@Component
public class ProfileObserverAccountCreated {
	
	@Autowired
	public ProfileObserverAccountCreated(){}
	
	@Autowired
	private ProfileRepository repository;
	
    @RabbitListener(queues = "#{accountCreated.name}")
    public void receive(Account account) throws InterruptedException {
        StopWatch watch = new StopWatch();
        watch.start();
      
        Profile profile = new Profile();
        profile.setBirthDate(account.getBirthdate());
        profile.setEmail(account.getEmail());
        profile.setDateCreated(DateTime.now().toString());
        profile.setName(account.getName());
        profile.setLastname(account.getLastname());
        profile.setStatus("CREATED");
        profile.setNickname(account.getName());
        profile.setAccount(account);
        repository.save(profile);
        System.out.println("instance [x] Received '" + profile.getEmail()  + profile.getBirthDate() +"'");
        //doWork(in);
        watch.stop();
        
    }

    private void doWork(String in) throws InterruptedException {
        for (char ch : in.toCharArray()) {
            if (ch == '.') {
                Thread.sleep(1000);
            }
        }
    }
}
