package social.media.profile.configuration;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProfileConfiguration {
    
	@Bean
    public Queue accountCreated() {
        return new Queue("AccountCreatedProfile");
    }

    @Bean
    public FanoutExchange fanout() {
        return new FanoutExchange("accountcreated.fanout");
    }
    @Bean
    public Binding binding1(FanoutExchange fanout,
        Queue accountCreated) {
        return BindingBuilder.bind(accountCreated).to(fanout);
    }
}
