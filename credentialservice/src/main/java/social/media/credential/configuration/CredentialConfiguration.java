package social.media.credential.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CredentialConfiguration {
	
	@Bean
    public Queue accountCreated() {
        return new Queue("AccountCreatedCredential");
    }

    @Bean
    public FanoutExchange fanout() {
        return new FanoutExchange("accountcreated.fanout");
    }
    @Bean
    public Binding binding1(FanoutExchange fanout,
        Queue accountCreated) {
        return BindingBuilder.bind(accountCreated).to(fanout);
    }

}
