package social.media.credential.rabbit;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import social.media.credential.repository.CredentialRepository;
import social.media.servicesharedmodels.account.Account;

@Component
public class CredentialObserverAccountCreated {
	@Autowired
    public CredentialObserverAccountCreated(){}
    
    @Autowired
	private CredentialRepository repository;
	
    @RabbitListener(queues = "#{accountCreated.name}")
    public void receive(Account account) throws InterruptedException {
        
        System.out.println("instance credential service [x] Received '" + account.getEmail() + "'");
     
    }

}
