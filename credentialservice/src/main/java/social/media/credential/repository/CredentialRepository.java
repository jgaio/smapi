package social.media.credential.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import social.media.credential.model.Credential;

public interface CredentialRepository extends MongoRepository <Credential,Long>{

}
