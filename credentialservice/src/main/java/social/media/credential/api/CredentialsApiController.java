package social.media.credential.api;

import io.swagger.annotations.*;
import social.media.credential.model.Credential;
import social.media.credential.restclient.Wso2TokenClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-10T13:51:10.272Z")

@Controller
public class CredentialsApiController implements CredentialsApi {

	@Autowired 
	Wso2TokenClientService tokenService;
	
    public ResponseEntity<Void> addCredential(@ApiParam(value = "Credential to add"  ) @RequestBody Credential credential) {
        // do some magic!
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

   

    public ResponseEntity<List<Credential>> searchCredential() {
        // do some magic!
    	List<Credential> result = new ArrayList<Credential>();
    	result.add(tokenService.buildCredential());
        return new ResponseEntity<List<Credential>>(result,HttpStatus.OK);
    }

	@Override
	public ResponseEntity<Credential> getCredentialByEmail(
			@ApiParam(value = "account email to find by credential",required=true ) @PathVariable("accountEmail") String accountEmail ,
			@ApiParam(value = "account name to find by credential",required=true ) @RequestParam("name") String name) {
		// TODO Auto-generated method stub
		return new ResponseEntity<Credential>(tokenService.buildCredential(),HttpStatus.OK);
	}

}
