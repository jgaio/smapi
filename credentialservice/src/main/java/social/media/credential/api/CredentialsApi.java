package social.media.credential.api;

import io.swagger.annotations.*;
import social.media.credential.model.Credential;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-10T13:51:10.272Z")

@Api(value = "credentials", description = "the credentials API")
public interface CredentialsApi {

    @ApiOperation(value = "adds a credential item", notes = "Adds a credential to the system", response = Void.class, tags={ "admins", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "item created", response = Void.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = Void.class),
        @ApiResponse(code = 409, message = "an existing item already exists", response = Void.class) })
    @RequestMapping(value = "/credentials",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> addCredential(@ApiParam(value = "Credential to add"  ) @RequestBody Credential credential);


    @ApiOperation(value = "Find credential by email", notes = "Returns a single credential", response = Credential.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Credential.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied", response = Credential.class),
        @ApiResponse(code = 404, message = "Credential not found", response = Credential.class) })
    @RequestMapping(value = "/credentials/{accountEmail}",
        produces = { "application/json" }, 
        method = RequestMethod.GET) 
ResponseEntity<Credential> getCredentialByEmail(@ApiParam(value = "account email to find by credential",required=true ) @PathVariable("accountEmail") String accountEmail,
		@ApiParam(value = "account name to find by credential",required=true ) @RequestParam("name") String name);


    @ApiOperation(value = "searches credential", notes = "Returns a list of credentials ", response = Credential.class, responseContainer = "List")
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "credentials", response = Credential.class),
        @ApiResponse(code = 400, message = "bad input parameter", response = Credential.class) })
    @RequestMapping(value = "/credentials",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Credential>> searchCredential();

}
