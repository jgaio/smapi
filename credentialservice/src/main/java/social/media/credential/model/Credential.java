package social.media.credential.model;

import java.io.Serializable;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Credential
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-10T13:51:10.272Z")

public class Credential implements Serializable  {
  @JsonProperty("email")
  private String email = null;

  @JsonProperty("password")
  private String password = null;

  @JsonProperty("login")
  private String login = null;

  @JsonProperty("status")
  private String status = null;

  @JsonProperty("dateCreated")
  private String dateCreated = null;

  @JsonProperty("dateStatus")
  private String dateStatus = null;

  @JsonProperty("dateChanged")
  private String dateChanged = null;

  public Credential email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "dev@social.media", required = true, value = "")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Credential password(String password) {
    this.password = password;
    return this;
  }

   /**
   * Get password
   * @return password
  **/
  @ApiModelProperty(example = "password", required = true, value = "")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Credential login(String login) {
    this.login = login;
    return this;
  }

   /**
   * Get login
   * @return login
  **/
  @ApiModelProperty(example = "default email", value = "")
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public Credential status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(example = "ativo/bloqueado", value = "")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Credential dateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
    return this;
  }

   /**
   * Get dateCreated
   * @return dateCreated
  **/
  @ApiModelProperty(example = "2016-08-29T09:12:33.001Z", value = "")
  public String getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Credential dateStatus(String dateStatus) {
    this.dateStatus = dateStatus;
    return this;
  }

   /**
   * Get dateStatus
   * @return dateStatus
  **/
  @ApiModelProperty(example = "2016-08-29T09:12:33.001Z", value = "")
  public String getDateStatus() {
    return dateStatus;
  }

  public void setDateStatus(String dateStatus) {
    this.dateStatus = dateStatus;
  }

  public Credential dateChanged(String dateChanged) {
    this.dateChanged = dateChanged;
    return this;
  }

   /**
   * Get dateChanged
   * @return dateChanged
  **/
  @ApiModelProperty(example = "2016-08-29T09:12:33.001Z", value = "")
  public String getDateChanged() {
    return dateChanged;
  }

  public void setDateChanged(String dateChanged) {
    this.dateChanged = dateChanged;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Credential credential = (Credential) o;
    return Objects.equals(this.email, credential.email) &&
        Objects.equals(this.password, credential.password) &&
        Objects.equals(this.login, credential.login) &&
        Objects.equals(this.status, credential.status) &&
        Objects.equals(this.dateCreated, credential.dateCreated) &&
        Objects.equals(this.dateStatus, credential.dateStatus) &&
        Objects.equals(this.dateChanged, credential.dateChanged);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, password, login, status, dateCreated, dateStatus, dateChanged);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Credential {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    login: ").append(toIndentedString(login)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
    sb.append("    dateStatus: ").append(toIndentedString(dateStatus)).append("\n");
    sb.append("    dateChanged: ").append(toIndentedString(dateChanged)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

