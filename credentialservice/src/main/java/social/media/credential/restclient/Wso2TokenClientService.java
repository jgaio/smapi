package social.media.credential.restclient;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import social.media.credential.model.Credential;
import social.media.servicesharedmodels.account.Account;

@Service
public class Wso2TokenClientService {
private final RestTemplate restTemplate;
	
	public Wso2TokenClientService(RestTemplateBuilder builder){
		this.restTemplate = builder.rootUri("https://localhost:8243").build();
	}
	
	public Credential buildCredential(){
		Credential cred = new Credential();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType( MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("grant_type", "password");
		//headers.add("Content-Type", "application/x-www-form-urlencoded");
		map.add("username", "dev");
		map.add("password", "kobop");
		headers.add("Authorization", "Basic ZjNtenQ5Q0hnaDZVMHZmeE9ZNlFZaHR2MmJVYTpwSEhGTEllSGw5YTJ5N2dxX3ZsYmJXR0p6ZWth");

		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map,headers);
		//restTemplate.exchange()
		//headers.setContentType(new MediaType("Application", "x-www-form-urlencoded"));
		
		
		String uri = "/token";
		ResponseEntity<String> resp = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		cred.setEmail(resp.getBody());
		return cred;
	}
}
