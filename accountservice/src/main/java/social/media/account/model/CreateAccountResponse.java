package social.media.account.model;

import java.util.Objects;

import social.media.servicesharedmodels.credential.Token;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-03T23:04:21.205Z")

public class CreateAccountResponse {
	
	private Token token;	
	
	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    CreateAccountResponse createAccountResponse = (CreateAccountResponse) o;
	    return true;
	    //return Objects.equals(this.name, createAccountRequest.name) &&
	//        Objects.equals(this.lastname, createAccountRequest.lastname) &&
	  //      Objects.equals(this.email, createAccountRequest.email) &&
	    //    Objects.equals(this.password, createAccountRequest.password) &&
	      //  Objects.equals(this.birthdate, createAccountRequest.birthdate);
	  }
	
	  @Override
	  public int hashCode() {
		  return 1;
	    //return Objects.hash(name, lastname, email, password, birthdate);
	  }
	
	  @Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class CreateAccountRequest {\n");
	    
	//	    sb.append("    name: ").append(toIndentedString(name)).append("\n");
	//	    sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
	//	    sb.append("    email: ").append(toIndentedString(email)).append("\n");
	//	    sb.append("    password: ").append(toIndentedString(password)).append("\n");
	//	    sb.append("    birthdate: ").append(toIndentedString(birthdate)).append("\n");
	    sb.append("}");
	    return sb.toString();
	  }
	
	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }
}
