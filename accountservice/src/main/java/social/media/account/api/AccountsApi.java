package social.media.account.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import social.media.account.model.CreateAccountRequest;
import social.media.account.model.CreateAccountResponse;
import social.media.servicesharedmodels.account.Account;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-03T23:04:21.205Z")

@Api(value = "accounts")
public interface AccountsApi {

    @ApiOperation(value = "adds an account", notes = "Adds an account to the system", response = Void.class, tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "account created", response = Void.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = Void.class),
        @ApiResponse(code = 409, message = "an existing account already exists", response = Void.class) })
    @RequestMapping(value = "/accounts",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<CreateAccountResponse> addAccount(@ApiParam(value = "Request to add an account"  ) @RequestBody CreateAccountRequest createAccountRequest);


    @ApiOperation(value = "Find account by email", notes = "Returns a single account", response = Account.class, tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = Account.class),
        @ApiResponse(code = 400, message = "Invalid ID supplied", response = Account.class),
        @ApiResponse(code = 404, message = "Account not found", response = Account.class) })
    @RequestMapping(value = "/accounts/{accountEmail}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<Account> getAccountByEmail(@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail") String accountEmail,
    		@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail2") String accountEmail2);


    @ApiOperation(value = "list accounts", notes = "Retrieve account list ", response = Account.class, responseContainer = "List", tags={ "developers", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "accounts", response = Account.class),
        @ApiResponse(code = 400, message = "bad input parameter", response = Account.class) })
    @RequestMapping(value = "/accounts",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<Account>> listAccounts();

}
