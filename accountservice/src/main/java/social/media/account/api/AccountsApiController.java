package social.media.account.api;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.ApiParam;
import social.media.account.model.CreateAccountRequest;
import social.media.account.model.CreateAccountResponse;
import social.media.account.repository.AccountRepositoryMongo;
import social.media.account.restclient.CredentialRestClientService;
import social.media.servicesharedmodels.account.Account;
import social.media.servicesharedmodels.credential.Token;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-03T23:04:21.205Z")

@Controller
public class AccountsApiController implements AccountsApi {
	
	@Autowired
	private AccountRepositoryMongo mrepository;
	
	@Autowired
	private CredentialRestClientService credentialRestClient;
	
    @Autowired
    private RabbitTemplate template;

    @Autowired
    private FanoutExchange fanout;
    
    public ResponseEntity<CreateAccountResponse> addAccount(@ApiParam(value = "Request to add an account"  ) @RequestBody CreateAccountRequest createAccountRequest) {
    	//checking for account existing
    	Iterable<Account> list = mrepository.findAll();
    	boolean existingAccount = false;
    	
    	if(!isAccountRequestValid(createAccountRequest)){
    	    return new ResponseEntity<CreateAccountResponse>(HttpStatus.BAD_REQUEST);
    	}
    	
    	for (Account account : list){
    		if (account.getEmail().equalsIgnoreCase(createAccountRequest.getEmail())){
    			existingAccount = true;
    		}
    	}
    	
    	if(!existingAccount){
	        // create and fill account object
	    	Account account = new Account();	    	
	    	account.setEmail(createAccountRequest.getEmail());
	    	account.setName(createAccountRequest.getName());
	    	account.setLastname(createAccountRequest.getLastname());
	    	account.setPassword(createAccountRequest.getPassword());
	    	account.setBirthdate(createAccountRequest.getBirthdate());
	    	account.setDateCreated(DateTime.now().toString());
	    	
	    	//save on mongo
	    	mrepository.save(account);
	    	//Json j =  (Json) account;
	    	//enqueue - throw event - account created 	
	    	
			template.convertAndSend(fanout.getName(),"", account);
			System.out.println(" [x] Sent '" + createAccountRequest.getName() + createAccountRequest.getEmail() + "'");
			
			//prepare response - get wso2 api token
			Account aaa = credentialRestClient.getAccount(createAccountRequest.getEmail());
	    	Token t = new Token();
	    	t.setScope(aaa.getEmail());
	    	//t.setToken("1233");
	    	CreateAccountResponse response = new CreateAccountResponse();
	    	response.setToken(t);
	        return new ResponseEntity<CreateAccountResponse>(response,HttpStatus.CREATED);
	        
    	}else{
    		return new ResponseEntity<CreateAccountResponse>(HttpStatus.CONFLICT);
    	}
    }

    private boolean isAccountRequestValid(CreateAccountRequest createAccountRequest) {
		if(createAccountRequest.getEmail() == null || createAccountRequest.getEmail().equals("") 
				|| createAccountRequest.getName() == null || createAccountRequest.getName().equals("") 
				|| createAccountRequest.getPassword() == null || createAccountRequest.getPassword().equals(""))
			return false;
		return true;
	}

	public ResponseEntity<Account> getAccountByEmail(@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail") String accountEmail,
			@ApiParam(value = "account email to find by",required=true ) @PathVariable("accountEmail2") String accountEmail2) {
        // do some magic!    	
        return new ResponseEntity<Account>(HttpStatus.OK);
    }

    public ResponseEntity<List<Account>> listAccounts() {
        // do some magic!
    	List<Account> result = (List<Account>) mrepository.findAll();
    	if (result == null){
    		result = new ArrayList<Account>();
    	}
        return new ResponseEntity<List<Account>>(result,HttpStatus.OK);
    }

}
