package social.media.account.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import social.media.servicesharedmodels.account.Account;

public interface AccountRepositoryMongo extends MongoRepository <Account, Long>{

    public Account findByEmail(String email);
    //public List<Account> findByName(String name);
}