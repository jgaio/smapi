package social.media.account.restclient;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import social.media.servicesharedmodels.account.Account;
import social.media.servicesharedmodels.credential.Token;


@Service
public class CredentialRestClientService {

	private final RestTemplate restTemplate;
	
	public CredentialRestClientService(RestTemplateBuilder builder){
		this.restTemplate = builder.rootUri("https://localhost:8243").build();
	}
	
	public Token getToken4CreatedAccount(String email){
		return this.restTemplate.getForObject("/credentials/1.0.0/credentials", Token.class, email);
		//return new Token();
	}
	
	public Account getAccount(String email){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Authorization", "Bearer f22469ea-8b10-3a5c-8c29-25e66f22a2a5");

		HttpEntity<String> entity = new HttpEntity<String>(email,headers);
		
		String uri = "/profiles/1.0.0/profiles/";
		ResponseEntity<Account> resp = restTemplate.exchange(uri, HttpMethod.GET, entity, Account.class);
		
		Account conta = resp.getBody();	

		return conta;

	}
}
