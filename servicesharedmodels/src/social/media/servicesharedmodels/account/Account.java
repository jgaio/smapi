package social.media.servicesharedmodels.account;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;


/**
 * Account
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-05-03T23:04:21.205Z")
@Entity
public class Account implements Serializable{
	@JsonProperty("id")
	  @Id
	  private String id = null;

	  @JsonProperty("email")
	  private String email = null;

	  @JsonProperty("dateCreated")
	  private String dateCreated = null;

	  @JsonProperty("name")
	  private String name = null;

	  @JsonProperty("lastname")
	  private String lastname = null;

	  @JsonProperty("password")
	  private String password = null;
	  
	  @JsonProperty("birthdate")
	  private String birthdate = null;

	  public Account id(String id) {
	    this.id = id;
	    return this;
	  }

	   /**
	   * Get id
	   * @return id
	  **/
	  @ApiModelProperty(value = "")
	  public String getId() {
	    return id;
	  }

	  public void setId(String id) {
	    this.id = id;
	  }

	  public Account email(String email) {
	    this.email = email;
	    return this;
	  }

	   /**
	   * Get email
	   * @return email
	  **/
	  @ApiModelProperty(value = "")
	  public String getEmail() {
	    return email;
	  }

	  public void setEmail(String email) {
	    this.email = email;
	  }

	  public Account dateCreated(String dateCreated) {
	    this.dateCreated = dateCreated;
	    return this;
	  }

	   /**
	   * Get dateCreated
	   * @return dateCreated
	  **/
	  @ApiModelProperty(example = "2016-08-29T09:12:33.001Z", value = "")
	  public String getDateCreated() {
	    return dateCreated;
	  }

	  public void setDateCreated(String dateCreated) {
	    this.dateCreated = dateCreated;
	  }


	  public String getName() {
		return name;
	  }
		
	  public void setName(String name) {
		this.name = name;
	  }

	  public String getLastname() {
		return lastname;
	  }
		
	  public void setLastname(String lastname) {
		this.lastname = lastname;
	  }
		
	  public String getBirthdate() {
		return birthdate;
	  }
		
	  public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	  }

	  public String getPassword() {
		return password;
	  }
		
	  public void setPassword(String password) {
		this.password = password;
	  }  
	  
	  @Override
	  public boolean equals(java.lang.Object o) {
	    if (this == o) {
	      return true;
	    }
	    if (o == null || getClass() != o.getClass()) {
	      return false;
	    }
	    Account account = (Account) o;
	    return Objects.equals(this.id, account.id) &&
	        Objects.equals(this.email, account.email) &&
	        Objects.equals(this.dateCreated, account.dateCreated);
	  }

	  @Override
	  public int hashCode() {
	    return Objects.hash(id, email, dateCreated);
	  }

	  @Override
	  public String toString() {
	    StringBuilder sb = new StringBuilder();
	    sb.append("class Account {\n");
	    
	    sb.append("    id: ").append(toIndentedString(id)).append("\n");
	    sb.append("    email: ").append(toIndentedString(email)).append("\n");
	    sb.append("    dateCreated: ").append(toIndentedString(dateCreated)).append("\n");
	    sb.append("}");
	    return sb.toString();
	  }

	  /**
	   * Convert the given object to string with each line indented by 4 spaces
	   * (except the first line).
	   */
	  private String toIndentedString(java.lang.Object o) {
	    if (o == null) {
	      return "null";
	    }
	    return o.toString().replace("\n", "\n    ");
	  }
}
